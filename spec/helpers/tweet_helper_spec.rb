require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the TweetHelper. For example:
#
# describe TweetHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe TweetHelper, type: :helper do
  context "Validar TweetService" do

    it "Checar se most relevants esta funcionando" do
      tweet = TweetHelper::TweetServices.new(ENV['HTTP_USERNAME'])
      expect(tweet.find_tweet("most_relevants")).to be_truthy
    end

    it "Checar se most mentions esta funcionando" do
      tweet = TweetHelper::TweetServices.new(ENV['HTTP_USERNAME'])
      expect(tweet.find_tweet("most_mentions")).to be_truthy
    end

  end
end
