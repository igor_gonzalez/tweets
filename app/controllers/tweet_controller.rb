class TweetController < ApplicationController
    layout false
    before_action :initialize_tweet_service

    #controler most_relevant
    def most_relevants
        @returns = @service.find_tweet("most_mentions")
    end

    #controler most_mentions
    def most_mentions 
        @returns = @service.find_tweet("most_mentions")
    end

    private
    def initialize_tweet_service
        @service = TweetHelper::TweetServices.new(ENV['HTTP_USERNAME'])
    end

end
