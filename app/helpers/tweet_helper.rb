module TweetHelper
    #classe dos serviços tweets
    class TweetServices
        private
        attr_reader :account_id, :base_url, :data
        
        #Contrutor da classe
        def initialize(account_id)
            @account_id = account_id
            @base_url   = 'http://tweeps.locaweb.com.br/tweeps'
        end

        #definição dos headers do HTTP
        def headers
            {
              'Accept': 'application/json',
              'Content-type': 'application/json',
              'Username': @account_id
            }
        end
        
        #Requisiçao GET da URL
        def entities
            url     = @base_url
            request = RestClient.get(url, headers)
            request = JSON.parse(request)
        end

        #Ordenamento do array
        def sort_by
            @data = @data.sort {|x,y| y[:favorite_count]<=>x[:favorite_count]}
            @data = @data.sort {|x,y| y[:retweet_count]<=>x[:retweet_count]}
            @data = @data.sort {|x,y| y[:followers_count]<=>x[:followers_count]}
        end

        #modelo do array para most_relevants
        def most_relevants(tweet)
            elements = 
                    {
                        followers_count: tweet["user"]["followers_count"],
                        screen_name: tweet["user"]["screen_name"],
                        profile_link: "https://twitter.com/"+tweet["user"]["screen_name"],
                        created_at: tweet["created_at"],
                        link: "https://twitter.com/"+tweet["user"]["screen_name"]+"/status/"+tweet["user"]["id_str"],
                        retweet_count: tweet["retweet_count"],
                        text: tweet["text"],
                        favorite_count: tweet["favorite_count"]
                    }
        end

        #modelo do array para most_mentions
        def most_mentions(tweet)
            
            elements = 
                        {tweet["user"]["screen_name"] => {
                            created_at: tweet["created_at"],
                            profile_link: "https://twitter.com/"+tweet["user"]["screen_name"],
                            favorite_count: tweet["favorite_count"],
                            screen_name: tweet["user"]["screen_name"],
                            followers_count: tweet["user"]["followers_count"],
                            link: "https://twitter.com/"+tweet["user"]["screen_name"]+"/status/"+tweet["user"]["id_str"],
                            text: tweet["text"],
                            retweet_count: tweet["retweet_count"]
                            }
                        }
        end

        public
        #Encontrar os tweet e as validacoes
        def find_tweet(eleme)
            @data = Array.new
            tweets = entities
            
            tweets["statuses"].each do |tweet|
                tweet["entities"]["user_mentions"].each do |mention|
                    # abort(mention)
                    if mention["id_str"] == ENV['HTTP_USERNAME'] && tweet["in_reply_to_user_id_str"] != ENV['HTTP_USERNAME']
                        if eleme == "most_relevants"
                            elements =  most_relevants(tweet)
                        elsif eleme == "most_mentions"
                            elements = most_mentions(tweet)
                        end
                        @data << elements
                    end    
                end
            end
            sort_by
            return @data  
        end
        
        #definição dos headers do HTTP
        def headers
            {
              'Accept': 'application/json',
              'Content-type': 'application/json',
              'Username': @account_id
            }
        end
    end

end
