# README

* Informaçao básica
Este projeto é um teste para avaliar a capacidade e o entendimento do uso de tweets atuais mais relevantes e os usuários que mais mencionam a Locaweb em tempo real.

* Tecnologias
Ruby version: 2.3.7
Rails version: 5.2.1

* Dependencia de Sistema
rest-client
jbuilder version: 2.5
puma version: 3.11
bootsnap version: 1.1.0


* Intruções de Acesso
- Most Relevants 
http://localhost:3000/tweet/most_relevants
- Most Mentions
http://localhost:3000/tweet/most_mentions

* Teste
Utilizando o rspec-rails
- Checar se most relevants esta funcionando
- Checar se most mentions esta funcionando


