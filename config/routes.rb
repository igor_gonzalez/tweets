Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # Routa padrao
  root 'tweet#most_relevants'
  get 'tweet/most_relevants'
  get 'tweet/most_mentions'
end
